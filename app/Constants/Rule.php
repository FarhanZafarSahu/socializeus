<?php
 
namespace App\Constants;

class Rule
{
    // Rules According to API's
	private static $rules = [
        'ADD_COMMENT' => [
            'event_id'      =>'exists:organise_events,id',
        ],
		'UPDATE_COMMENT' => [
		'id'  => 'exists:comments,id',
			],
		'DELETE_COMMENT' => [
		'id'  => 'exists:comments,id',
			],
		'ADD_EVENT_PARTICIPENT' => [
            'event_id'      =>'exists:organise_events,id',
			]
        ];

	public static function get($api){
		return self::$rules[$api];
	}
}