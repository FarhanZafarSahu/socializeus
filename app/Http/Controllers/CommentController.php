<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
// Constants
use App\Constants\ResponseCode;
use App\Constants\Message;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request , $id)
    {
        try 
        {
                $comments = Comment::where('event_id',$id)
                                ->with('User')
                                ->with('Event')
                                ->get();
            $response = makeResponse(ResponseCode::SUCCESS, Message::REQUEST_SUCCESSFUL, true, null, $comments);
        }
        catch (\Exception $e) 
        {
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }    
        return $response;    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validateData($request,'ADD_COMMENT');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        
        \DB::beginTransaction();
        try 
        {
			$image_path = "";
			$video_path = "";
            $user = \Auth::user();
		if ($request->hasFile('video_url'))
		{
			$propertyThumbnailDirectory = 'public/comment';
			if (!\Storage::exists($propertyThumbnailDirectory))
			{
				\Storage::makeDirectory($propertyThumbnailDirectory);
			}
			$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
			$video_path = str_replace(array("public/"), "", $thumbnailUrl);	
		}
		if ($request->hasFile('photo_url'))
		{
			$propertyThumbnailDirectory = 'public/comment';
			if (!\Storage::exists($propertyThumbnailDirectory))
			{
				\Storage::makeDirectory($propertyThumbnailDirectory);
			}
			$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
			$image_path = str_replace(array("public/"), "", $thumbnailUrl);	
		}
	
            $comm =  Comment::create([
                                "event_id" => $request->event_id,
                                "user_id" => $user->id ?? '',
                                "comment_text" => $request->comment_text,
                                "location_url" => $request->location_url,
								"photo_url"   => $image_path,
								"video_url" => $video_path,
            ]);                    
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS, "Comment Added.", true, null, $comm);
        }
        catch (\Exception $e) 
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;         
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request)
    {         
        $validator = validateData($request,'UPDATE_COMMENT');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        
        \DB::beginTransaction();
        try 
        {
			$user = \Auth::user();
		    $comment = Comment::where('id', $request->id)->first();
			$video_path = $comment->video_url;
			$image_path = $comment->photo_url;
			$comment_text = isset($request->comment_text) ? $request->comment_text : $comment->comment_text;
			$location_url = isset($request->location_url) ? $request->location_url : $comment->location_url;
			if ($request->hasFile('video_url'))
			{
				$propertyThumbnailDirectory = 'public/comment';
				if (!\Storage::exists($propertyThumbnailDirectory))
				{
					\Storage::makeDirectory($propertyThumbnailDirectory);
				}
				$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
				$video_path = str_replace(array("public/"), "", $thumbnailUrl);	
			}
			if ($request->hasFile('photo_url'))
			{
				$propertyThumbnailDirectory = 'public/comment';
				if (!\Storage::exists($propertyThumbnailDirectory))
				{
					\Storage::makeDirectory($propertyThumbnailDirectory);
				}
				$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
				$image_path = str_replace(array("public/"), "", $thumbnailUrl);	
			}
            Comment::where('id', $request->id)->update([
                                "user_id" => $user->id ?? '',
                                "location_url" => $location_url,
								"photo_url"   => $image_path,
								"video_url" => $video_path,
                                "comment_text" => $comment_text,                                              
                                ]);
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS, "Comment Updated Successfully.", true);
        }
        catch (\Exception $e) 
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
     public function deleteComment(Request $request)
    {  
        $validator = validateData($request,'DELETE_COMMENT');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        
        \DB::beginTransaction();
        try 
        {
            $id          =   $request->id;
            $comment = Comment::find($id);
            
            if(empty($comment))
                return $response = makeResponse(ResponseCode::FAIL,Message::RECORD_NOT_FOUND,false,null);  
            
            $comment->delete();
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS, "Comment Deleted Successfully.", true);
        }
        catch (\Exception $e) 
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;
    }
}
