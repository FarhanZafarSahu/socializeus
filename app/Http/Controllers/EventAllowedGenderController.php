<?php

namespace App\Http\Controllers;

use App\Models\EventAllowedGender;
use Illuminate\Http\Request;

class EventAllowedGenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventAllowedGender  $eventAllowedGender
     * @return \Illuminate\Http\Response
     */
    public function show(EventAllowedGender $eventAllowedGender)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventAllowedGender  $eventAllowedGender
     * @return \Illuminate\Http\Response
     */
    public function edit(EventAllowedGender $eventAllowedGender)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventAllowedGender  $eventAllowedGender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventAllowedGender $eventAllowedGender)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventAllowedGender  $eventAllowedGender
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventAllowedGender $eventAllowedGender)
    {
        //
    }
}
