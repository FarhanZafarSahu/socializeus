<?php

namespace App\Http\Controllers;

use App\Models\event_allowed_paymentMethod;
use Illuminate\Http\Request;

class EventAllowedPaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\event_allowed_paymentMethod  $event_allowed_paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(event_allowed_paymentMethod $event_allowed_paymentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\event_allowed_paymentMethod  $event_allowed_paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(event_allowed_paymentMethod $event_allowed_paymentMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\event_allowed_paymentMethod  $event_allowed_paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, event_allowed_paymentMethod $event_allowed_paymentMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\event_allowed_paymentMethod  $event_allowed_paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(event_allowed_paymentMethod $event_allowed_paymentMethod)
    {
        //
    }
}
