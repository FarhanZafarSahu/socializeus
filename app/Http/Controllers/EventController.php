<?php

namespace App\Http\Controllers;

use App\Models\{OrganiseEvent,Event,EventAllowedGender,EventAllowedPaymentMethod,EventParticipent};
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEventRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventRequest $request)
    {
		\DB::beginTransaction();
		$user = \Auth::user();
		$image = NULL;
		$event_type = isset($request->event_country) ? 0 : (isset($request->event_url) ? 2 : 1);
		$payment_type = isset($request->payable_amount) ? 1:0;
		if ($request->hasFile('image'))
		{
			$propertyThumbnailDirectory = 'public/events';
			if (!\Storage::exists($propertyThumbnailDirectory))
			{
				\Storage::makeDirectory($propertyThumbnailDirectory);
			}
			$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));

			$image = $thumbnailUrl;
		$image = str_replace(array("public/"), "", $image);
		}
		// Date Calculation.
 		$s = $request->event_start_date;
		$date = strtotime($s);
		$start_date = date('Y-m-d H:i:s', $date);
		$end_date = strtotime($start_date.'+'.$request->duration.' hours');
		$end_date = date('Y-m-d H:i:s', $end_date);
		
		$eventobj = new OrganiseEvent();
		$eventobj->user_id = $user->id;
		$eventobj->event_name = $request->event_name;
		$eventobj->event_type = $event_type;
		$eventobj->event_address = $request->event_address;
		$eventobj->event_country = $request->event_country;
		$eventobj->event_start_date = $start_date;
		$eventobj->event_end_date = $end_date;
		$eventobj->image = $image;
		$eventobj->event_description = $request->event_description;
		$eventobj->find_us_description = $request->find_us_description;
		$eventobj->payment_type = $payment_type;
		$eventobj->event_amount = $request->payable_amount;
		$eventobj->payable_type = $request->payable_type;
		$eventobj->no_of_participent = $request->no_of_participent;
		$eventobj->event_min_age = $request->event_min_age;
		$eventobj->event_max_age = $request->event_max_age;
		$eventobj->save();
		if(isset($request->gender) && count($request->gender) != 0)
		{
			foreach($request->gender as $g)
			{
				$eventAllowedGender = new EventAllowedGender();
				$eventAllowedGender->gender_id = $g;
				$eventAllowedGender->event_id = $eventobj->id;
				$eventAllowedGender->save();
			}
		}
		if($payment_type == 1 && count($request->payment_methods) != 0)
		{
			foreach($request->payment_methods as $payments)
			{
					$eventAllowedGender = new EventAllowedPaymentMethod();
					$eventAllowedGender->payment_method_id = $payments;
					$eventAllowedGender->event_id = $eventobj->id;
					$eventAllowedGender->save();
			}
		}
		\DB::commit();
		return \Redirect::back()->with('success', 'Request Successfull');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event,$id)
    {
        $event = OrganiseEvent::with('paymentMethods','genders')->findOrFail($id);
		$from = $event->event_start_date;
		$to = $event->event_end_date;
		$event->diff_in_hours = $from->diffInHours($to);
		// dd($event);
		 return view('events.edit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEventRequest  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventRequest $request)
    {
		\DB::beginTransaction();
		$user = \Auth::user();
			// Date Calculation.
 		$s = $request->event_start_date;
		$date = strtotime($s);
		$start_date = date('Y-m-d H:i:s', $date);
		$end_date = strtotime($start_date.'+'.$request->duration.' hours');
		$end_date = date('Y-m-d H:i:s', $end_date);
		$eventobj = OrganiseEvent::where('id',$request->id)->first();
		$image = $eventobj->image;
		$event_type = isset($request->event_country) ? 0 : (isset($request->event_url) ? 2 : 1);
		$payment_type = isset($request->payable_amount) ? 1:0;
		if ($request->hasFile('image'))
		{
			$propertyThumbnailDirectory = 'public/events';
			if (!\Storage::exists($propertyThumbnailDirectory))
			{
				\Storage::makeDirectory($propertyThumbnailDirectory);
			}
			$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));

			$image = $thumbnailUrl;
			$image = str_replace(array("public/"), "", $image);
		}

		$eventobj->user_id = $user->id;
		$eventobj->event_name = $request->event_name;
		$eventobj->event_type = $event_type;
		$eventobj->event_address = $request->event_address;
		$eventobj->event_country = $request->event_country;
		$eventobj->event_start_date = $start_date;
		$eventobj->event_end_date = $end_date;
		$eventobj->image = $image;
		$eventobj->event_description = $request->event_description;
		$eventobj->find_us_description = $request->find_us_description;
		$eventobj->payment_type = $payment_type;
		$eventobj->event_amount = $request->payable_amount;
		$eventobj->payable_type = isset($request->payable_type) ? $request->payable_type : $eventobj->payable_type;
		$eventobj->no_of_participent = $request->no_of_participent;
		$eventobj->event_min_age = $request->event_min_age;
		$eventobj->event_max_age = $request->event_max_age;
		$eventobj->save();
		if(isset($request->gender) && count($request->gender) != 0)
		{
			EventAllowedGender::where('event_id',$request->id)->delete();
			foreach($request->gender as $g)
			{
				$eventAllowedGender = new EventAllowedGender();
				$eventAllowedGender->gender_id = $g;
				$eventAllowedGender->event_id = $request->id;
				$eventAllowedGender->save();
			}
		}
		if($payment_type == 1 && (count($request->payment_methods) != 0))
		{
			EventAllowedPaymentMethod::where('event_id',$request->id)->delete();
			foreach($request->payment_methods as $payments)
			{
					$eventAllowedGender = new EventAllowedPaymentMethod();
					$eventAllowedGender->payment_method_id = $payments;
					$eventAllowedGender->event_id = $request->id;
					$eventAllowedGender->save();
			}
		}
		\DB::commit();
		return \Redirect::back()->with('success', 'Request Successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
    public function detail($id)
    {
        $event = OrganiseEvent::findOrFail($id);
		$participent = EventParticipent::where('event_id',$id)->where('user_id',\Auth::user()->id)->count();
        $event->not_a_participent = $participent;
		return view('events.single', compact('event'));
    }
}
