<?php

namespace App\Http\Controllers;

use App\Models\EventParticipent;
use Illuminate\Http\Request;
// Constants
use App\Constants\ResponseCode;
use App\Constants\Message;

class EventParticipentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index(Request $request , $id)
    {
        try
        {
                $eventParticipent = EventParticipent::where('event_id',$id)->with('event.user')->get();
            $response = makeResponse(ResponseCode::SUCCESS, Message::REQUEST_SUCCESSFUL, true, null, $eventParticipent);
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $validator = validateData($request,'ADD_EVENT_PARTICIPENT');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);

        \DB::beginTransaction();
        try
        {
            $user = \Auth::user();
            $eventParticipent =  EventParticipent::create([
                                "event_id" => $request->event_id,
                                "user_id" => $user->id ?? '',
            ]);
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS, "user Participated", true, null, $eventParticipent);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventParticipent  $eventParticipent
     * @return \Illuminate\Http\Response
     */
    public function show(EventParticipent $eventParticipent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventParticipent  $eventParticipent
     * @return \Illuminate\Http\Response
     */
    public function edit(EventParticipent $eventParticipent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventParticipent  $eventParticipent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventParticipent $eventParticipent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventParticipent  $eventParticipent
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventParticipent $eventParticipent)
    {
        //
    }
}
