<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\OrganiseEvent;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events = OrganiseEvent::with('genders','paymentMethods','eventParticipent')->get();
        return view('home',compact('events'));
    }
}
