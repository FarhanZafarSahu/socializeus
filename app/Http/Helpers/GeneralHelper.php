<?php

// Constants
use App\Constants\General;

// Models
use App\Constants\Message;use App\Constants\ResponseCode;
use App\Models\Cart;
use App\Models\City;
use App\Models\PaymentRecord;

// Constants

use App\Models\Recipt;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Concat two string.
 *
 * @param  string $strA
 * @param  string $strB
 * @param  string $strBetween (optional)
 * @return string
 */
function concatTwoString($strA, $strB, $strBetween = '')
{
    return $strA . $strBetween . $strB;
}

/**
 * Generate SD,ED Range.
 *
 * @param  integer $filter
 * @param  string $SD
 * @param  string $ED
 * @param  string $schoolTimeZone
 * @return array
 */
function generateActivityDatesRange($filter, $customSD, $customED, $schoolTimeZone)
{
    $SD = $ED = "";
    $format = General::DATE_FORMAT_1;

    if ($filter != General::FILTER_CUSTOM) {
        list($SD, $ED) = getDateRange(date($format, strtotime($schoolTimeZone)), $filter);
    } else {
        $SD = carbonDate($customSD, $format);
        $ED = carbonDate($customED, $format);
    }
    return [$SD, $ED];
}

function calculatePercentage($valueA, $valueB, $decimalPlaces = 2)
{
    $result = ($valueA / $valueB) * 100;
    return number_format($result, $decimalPlaces, '.', '');
}

function attemptJWTLogin($credentials, $roleType = 0, $FCMToken = null, $macAddress = null)
{
    $token = null;
    try
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            return getInvalidCredentialsResponse();
        }

    } catch (JWTAuthException $e) {
        return getTokenFailureResponse();
    }
    $user = JWTAuth::user();
    
    
    if ($user->roleId == 6 )
    {
      
        $superadmin = User::where('roleId',1)->first();
    }
    //TODO
    // if ($roleType == 1 && $user->roleId == 2) {
    //     JWTAuth::setToken($token);
    //     JWTAuth::invalidate();
    //     return makeResponse(ResponseCode::FAIL, Message::PERMISSION_DENIED, false);
    // }
    // ENDTODO

    if ($user->isUserDeleted()) {
        return makeResponse(ResponseCode::FAIL, Message::USER_BLOCK, false);
    }

    if (isset($FCMToken)) {
        $user->updateFcmToken($FCMToken, $macAddress);
    }

    $result = [
        'token' => $token,
        'info' => $user->getArrayResponse(),
        'city' => $user->city,
        'productInCart' => Cart::where('userId', $user->id)->count(),
        'noOfRecipts' => Recipt::where('sellerId', $user->id)->where('status', '!=', 0)->count(),
        'noOfUnreadThread' => $user->undreadMessageCount(),
        'pendingOrder' => getPendingOrders($user->id),
        'totalSoldProducts' => $user->getTotalSoldProduct(),
        'superAdminId' => $superadmin->id ?? 0
    ];

    return makeResponse(ResponseCode::SUCCESS, Message::REQUEST_SUCCESSFUL, true, null, $result);
}

function linkActivator($route)
{
    return (strpos(Route::getCurrentRoute()->uri(), $route));
}

function customIsset($value)
{
    if ($value != null && $value != "" && $value != "NULL" && $value != "null") {
        return true;
    }

    return false;
}

function paginate($items, $perPage = 5, $page = null)
{
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $total = count($items);
    $currentpage = $page;
    $offset = ($currentpage * $perPage) - $perPage ;
    $itemstoshow = array_slice($items , $offset , $perPage);
    return new LengthAwarePaginator($itemstoshow ,$total ,$perPage,   $page,
    [
        'path' => LengthAwarePaginator::resolveCurrentPath(),
    ]);
}
function csvToArray($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}
function deepLink($id)
{
    try
    {
        $fbResult = [];
        $val = "https://gahhakapp.page.link/?link=https://gahhak.pk/product-page?id=".$id;
        $data = [
            'longDynamicLink' => $val
        ];
        $dataString = json_encode($data);
        $headers = [
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyBQzMLj8hE3NYyn5ThXlhcB4l___b23as4');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $fbResults = curl_exec($ch);
        return $fbResults;
    }
    catch (\Exception $e) 
    {
        $fbResults =
        [
         'shortLink' => NULL,
        ];
        
        return json_encode($fbResults);
    }
}

function permission($page, $roleId)
{

    $adminArray = $superAdminArray = $accountantArray = [
        'account',
        'users',
        'user-receivable',
        'imageInspection',
        'QR',
        'top-up',
        'feedback',
        'logistic-payable',
        'sales',
        'report',
        'block',
        'order',
        'order/list',
        'order/placed',
        'order/canceled',
        'order/confirmed',
        'order/pickedup',
        'order/complete',
        'order/variance',
        'order/detail',
        'order/track',
        'coupon',
        'forrun',
        'list',
        'withdraw-request',
        'story/add',
        'banners/index',
        'QR/list',
        '/products/list',
    ];
    array_push($superAdminArray, 'notification-marketing');
    array_push($adminArray, 'notification-marketing');
    array_push($superAdminArray, 'email-marketing');
    array_push($adminArray, 'email-marketing');
    array_push($superAdminArray, 'popups/index');
    array_push($adminArray, 'popups/index');
    $marketingArray = [
        'sales',
        'Marketing/Notification',
        'email-marketing',
        'notification-marketing',
        'banners/index',
        'popups/index',
        '/products/list',
    ];
    $customerRepresentativeArray = [
        'users',
        'sales',
        'imageInspection',
        'QR',
        'top-up',
        'feedback',
        'sale-report',
        'report',
        'block',
        'order',
        'order/list',
        'order/placed',
        'order/canceled',
        'order/confirmed',
        'order/pickedup',
        'order/complete',
        'order/variance',
        'order/detail',
        'order/track',
        'coupon',
        'forrun',
        'list',
        'withdraw-request',
        'Marketing/Notification',
        'email-marketing',
        'notification-marketing',
        'banners/index',
        'popups/index',
        '/products/list',
    ];
    $investorArray = [
            'users',
    ];
    $forrunArray = [
        'order',
        'order/track',
        'order/confirmed',
        'order/pickedup',
        'order/complete',
    ];
    if (in_array($page, $adminArray) && $roleId == 4) {
        return true;
    }
    if (in_array($page, $superAdminArray) && $roleId == 1) {
        return true;
    }
    if (in_array($page, $accountantArray) && $roleId == 8) {
        return true;
    }
    if (in_array($page, $marketingArray) && $roleId == 5) {
        return true;
    }
    if (in_array($page, $customerRepresentativeArray) && $roleId == 6) {
        return true;
    }
    if (in_array($page, $investorArray) && $roleId == 7) {
        return true;
    }
    if (in_array($page, $forrunArray) && $roleId == 3) {
        return true;
    }
    return false;
}
function escape_like($string)
{
    $search     = array('%', '_','#','&');
    $replace    = array('\%', '\_', '\#','\&');

    return str_replace($search, $replace, $string);
}
function DeductPayment($amount,$user,$paymentType)
{
	if($paymentType == 1)  // payment through Gahhak wallet
	{
		if($amount > $user->eWallet)  // payment through Gahhak wallet
			return $response = makeResponse(ResponseCode::FAIL,Message::LOW_WALLET_BALANCE,false,null);
		User::where('id',$user->id)->decrement('eWallet', $amount);
		return true ;
	}
	return false;
}