<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
 	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'event_id',
        'comment_text',
        'video_url',
        'photo_url',
		'location_url'
    ];

    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {
        
        return [

            'user_id'     => $this->user_id,
            'event_id'     => $this->event_id,
            'comment_text' => $this->comment_text,
            'video_url'    => $this->video_url,
            'photo_url'    => $this->image_url,
 			'location_url' => $this->location_url,
        ];
    }
  /**
     * Realtion to "events" Table.
     *
     */
    public function Event()
    {
        return $this->belongsto(Event::class,'event_id','id');
    }

    /**
     * Realtion to "user" Table.
     *	
     */
    public function User()
    {
        return $this->belongsto(User::class,'user_id','id');
    }
   
  
}
