<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventAllowedPaymentMethod extends Model
{
    use HasFactory;
	protected $table = 'event_allowed_payment_methods';
}
