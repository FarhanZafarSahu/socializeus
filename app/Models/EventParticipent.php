<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventParticipent extends Model
{
    use HasFactory;
const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'event_id',
          ];

    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {
        
        return [

            'user_id'     => $this->user_id,
            'event_id'     => $this->event_id,
        ];
    }
	public function event()
    {
        return $this->belongsto(OrganiseEvent::class,'event_id','id');
    }
}
