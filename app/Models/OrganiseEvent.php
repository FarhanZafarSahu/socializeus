<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganiseEvent extends Model
{
    use HasFactory;
	protected $dates = ['event_start_date', 'event_start_date'];
 	public function genders()
    {
        return $this->hasone(EventAllowedGender::class,'event_id','id');
    }
 	public function paymentMethods()
    {
        return $this->hasone(EventAllowedPaymentMethod::class,'event_id','id');
    }
	public function eventParticipent()
    {
        return $this->hasMany(EventParticipent::class,'event_id','id');
    }
 	public function user()
    {
        return $this->belongsTo(User::class);
    }
}
