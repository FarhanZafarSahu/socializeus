<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganiseEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organise_events', function (Blueprint $table) {
            $table->id();
			$table->integer('user_id');
			$table->string('event_name')->nullable();
			$table->integer('event_type')->comment('0 => address, 1 => online, 2 => googleMapUrl')->default(0);
			$table->string('event_address')->nullable();
			$table->string('event_country')->nullable();
			$table->timestamp('event_start_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('event_end_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('image')->nullable();
			$table->string('event_description')->nullable();
			$table->string('find_us_description')->nullable();
			$table->integer('payment_type')->comment('0 => free, 1 => paid,')->default(0);
			$table->integer('payable_type')->comment('0 => online, 1 => offline,')->default(0);
			$table->integer('no_of_participent')->nullable();
			$table->integer('event_min_age')->nullable();
			$table->integer('event_max_age')->nullable();
			$table->integer('event_url')->nullable();
			$table->integer('event_amount')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organise_events');
    }
}
