@extends('auth.layouts.app')
@section('styles')
    <style>
        body{

            background:linear-gradient(0deg, rgba(255, 0, 150, 0.3), rgba(255, 0, 150, 0.3)), url("{{asset('images/crowd3.png')}}");
            background-repeat: no-repeat;
            position: center  ;
            background-size: cover;
        }
    </style>
@endsection
@section('content')
    <div class="registration-form">
        <form action="{{route('login')}}" method="POST">
            @csrf
            <div class="form-icon" style=" background-color: limegreen;">
                <span><i class="fas fa-sign-in-alt"></i></span>
            </div>
            <div>
                <p class="text-center" style="color: green; font-family: 'Segoe UI'; font-weight: 400">Please Fill The Details To Get In</p>
            </div>

            <div class="form-group">
                <input type="email" id="email" placeholder="email" class="form-control item  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="form-group">
                <input id="password" type="password" class="form-control item  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block   create-account" style="background-color: limegreen ">Login</button>
            </div>

        </form>
        <div class="social-media">
            <h5>Sign In with social media</h5>
            <div class="social-icons">
                <a href="#"><i class="fab fa-facebook"  ></i></a>
                <a href="#"><i class="fab fa-instagram"  ></i></a>
                <a href="#"><i class="fab fa-twitter"  ></i></a>
            </div>
        </div>
    </div>

    <script src="{{asset('assets/js/script.js')}}"></script>
@endsection
