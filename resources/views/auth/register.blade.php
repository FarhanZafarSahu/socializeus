@extends('auth.layouts.app')
@section('styles')
    <style>
        body{

            background:linear-gradient(0deg, rgba(255, 0, 150, 0.3), rgba(255, 0, 150, 0.3)), url("{{asset('images/crowd3.png')}}");
            background-repeat: no-repeat;
            position: center  ;
            background-size: cover;
        }
    </style>
@endsection
@section('content')
    <div class="registration-form">
        <form action="{{route('register')}}" method="POST">
            @csrf
            <div class="form-icon" style=" background-color: limegreen;">
                <span><i class="fas fa-user-plus" style=""></i></span>
            </div>
            <div>
                <p class="text-center" style="color: green; font-family: 'Segoe UI'; font-weight: 400">Please Fill The Details To Get Registered</p>
            </div>
            <div class="form-group">
                <input type="text"   id="name" placeholder="name" class="form-control item   @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="email" placeholder="email" class="form-control item  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password"  placeholder="password" class="form-control item @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password-confirm" type="password" placeholder=" confirm password" class="form-control item" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block   create-account" style="background-color: limegreen ">Create Account</button>
            </div>
        </form>
        <div class="social-media">
            <h5>Sign up with social media</h5>
            <div class="social-icons">
                <a href="#"><i class="fab fa-facebook"  ></i></a>
                <a href="#"><i class="fab fa-instagram"  ></i></a>
                <a href="#"><i class="fab fa-twitter"  ></i></a>
            </div>
        </div>
    </div>

    <script src="{{asset('assets/js/script.js')}}"></script>
@endsection
