@extends('layouts.app')
@section('title', 'Create Event')
@section('styles')
<style>

</style>
@endsection
@section('content')
<div class="container">

@include('layouts.displayMessages')
    <div class="row">
        <div class="col-md-6 offset-3">
        <form class="form-horizontal" method="POST" action="{{route('event.store')}}" enctype="multipart/form-data">
            @csrf
<fieldset>

<!-- Form Name -->
<legend>Create New Event</legend>

<!-- name input-->
<div class="form-group">
  <label class="control-label" for="event_name">Name*</label>  
  <div class="">
  <input id="event_name" name="event_name" type="text" placeholder="Event Name" class="form-control input-md">
    
  </div>
</div>
<div class="form-group">
  <label class="control-label" for="event_name">Type Of Event*</label>  
  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Address</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Online</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Google Maps</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
      <input type="text" name="event_address" id="address" placeholder="Enter your address" class="form-control">
      <input type="text" name="event_country" id="country" placeholder="Which country you belongs to" class=" mt-2 form-control">
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
  <textarea name="online_address" id="address" placeholder="Please describe how and where you will manage the event " class="form-control"></textarea>
  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
  <input type="text" name="event_url" id="google_map_address" placeholder="Enter google map URL of you location" class="form-control">
  </div>
</div>
</div>
<!-- date and hour -->
<div class="form-group">
  <label class=" control-label" for="textinput">Date and Time*</label>  
  <div class="">
  <input id="event_date" name="event_start_date" type="datetime-local" placeholder="event date" class="form-control input-md">
  </div>
</div>
<div class="form-group">
  <label class=" control-label" for="textinput">Duration*</label>  
  <div class="">
<select class = "form-control" name = "duration">
    <option value="1">1 hours</option>
    <option selected value="2">2 hours</option>
	<option value="3">3 hours</option>
    <option value="4">4 hours</option>
	<option value="5">5 hours</option>
    <option value="6">6 hours</option>
	<option value="7">7 hours</option>
    <option value="8">8 hours</option>
	<option value="9">9 hours</option>
    <option value="10">10 hours</option>
</select>  
  </div>
</div>
<!-- image -->
<div class="form-group">
  <label class=" control-label" for="textinput">Please Select Image</label>  
  <div class="">
  <input id="image" name="image" type="file" placeholder="select image" class="form-control input-md">
  </div>
</div>
<!-- description -->
<div class="form-group">
  <label class=" control-label" for="description">Event Description</label>
  <div class="">                     
    <textarea class="form-control" id="description" name="event_description"></textarea>
  </div>
</div>

<!-- find us -->
<div class="form-group">
  <label class=" control-label" for="find_us">How to find us</label>
  <div class="">                     
    <textarea class="form-control" id="find_us" name="find_us_description"></textarea>
  </div>
</div>
<!-- payment block -->
<div class="form-group">
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Free</a>
    <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Payable</a>
   
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <label for="quantity" class="mt-2">Enter te capacity</label>
      <input type="number" class="form-control" name="quantity" id="quantity" min=0 >
  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
      <input type="number" onkeyup="calculateFee()" name="payable_amount" id="payable_amount" placeholder="Enter amonut" class="m-3">
      <div class="">
<div id="myRadioGroup" class="ml-3">
    <input type="radio" name="payable_type" checked="checked" value="0" class="pr-2" />Yes
    <input type="radio" name="payable_type" value="1" />No

    <div id="payable_type0" class="desc">
        <strong>5% Fee will be charged : </strong><span id="fee" >0.00</span>
    </div>
    <div id="payable_type1" class="desc" style="display: none;">
       <label for="select_mathod">Select Your Possible Mathods </label>
       <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="payment_methods[]" value="0">
  <label class="form-check-label" for="inlineCheckbox1">Cash</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="payment_methods[]" value="1">
  <label class="form-check-label" for="inlineCheckbox2">Card</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="payment_methods[]" value="1">
  <label class="form-check-label" for="inlineCheckbox3">Cheque</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="payment_methods[]" value="2">
  <label class="form-check-label" for="inlineCheckbox3">Others</label>
</div>

    </div>
    <label for="quantity" class="mt-2">Enter te capacity</label>
      <input type="number" class="form-control" name="no_of_participent" id="quantity" min=0 >
</div>

  </div>
</div>
</div>


<!-- Select Basic -->
<div class="form-group mt-2">
  <label class=" control-label" for="selectbasic">Gander Allowed</label>
  <div class="">
       <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="gender[]" value="1">
  <label class="form-check-label" for="inlineCheckbox1">Male</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="gender[]" value="2">
  <label class="form-check-label" for="inlineCheckbox2">Female</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="gender[]" value="3">
  <label class="form-check-label" for="inlineCheckbox3">Other</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="gender[]" value="0">
  <label class="form-check-label" for="inlineCheckbox3">Not Specified</label>
</div>

</div>
  </div>
</div>

<!-- ages -->
<div class="form-group">
  <label class=" control-label" for="textarea">Minimum Age</label>
  <div class="">                     
    <input type="number" name="event_min_age" id="min_age" class="form-control" min=0 placeholder="minimum age">
  </div>
</div>
<div class="form-group">
  <label class=" control-label" for="textarea">Maximum Age</label>
  <div class="">                     
    <input type="number" name="event_max_age" id="max_age" class="form-control" min=0 placeholder="maximum age">
  </div>
</div>
{{-- <div class="form-group">
  <label class=" control-label" for="textarea">URL</label>
  <div class="">                     
    <input type="text" name="url" id="url" class="form-control" placeholder="https://www.example.com">
  </div>
</div> --}}
<div class="form-group">
 
  <div class="">                     
    <input type="submit" class="btn btn-success" id="url" class="form-control" value="Post the Event">
  </div>
</div>
</fieldset>
</form>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function calculateFee()
    {
        var payable_amount = document.getElementById('payable_amount').value;
        var fee = (5/100)*payable_amount;
        document.getElementById('fee').innerHTML=fee;
    }
</script>
<script>
$(document).ready(function() {
    $("input[name$='payable_type']").click(function() {
        var test = $(this).val();
        $("div.desc").hide();
        $("#payable_type" + test).show();
    });
});
</script>
@endsection