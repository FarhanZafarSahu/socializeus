@extends('layouts.app')
@section('title', 'Create Event')
@section('styles')
    <style>

    </style>
@endsection
@section('content')
    <div class="container">

        @include('layouts.displayMessages')
        <div class="row">
            <div class="col-md-8 offset-2">
                <div class="card mb-3">
                    <img src="{{env('IMAGE_PATH').'storage/app/public/'.$event->image}}" style="height: 150px; width: 100% ; margin-top: 10px;">
                    <div class="card-body">
	@if($event->not_a_participent == 0 )
	 <form id="participate_form" name="participate_form">
             <input type="hidden" name="event_id" id="event_id" value="{{$event->id}}"/>

 				@csrf

                     <button type="submit" id="participateBtn" value="Edit" class="btn btn-success">want to Particiapte</button>
</form>
@else
                     <button id = "hiddenbtn" value="participated">Participated</button>

@endif
                     <button id = "hiddenbtn" style = "display:none;" value="participated">Participated</button>

         @if(Auth::id()==$event->user_id)
                              <a href="{{route('event.edit',$event->id)}}" class="btn btn-success" style="float: right">Edit This Event</a>
                            @endif
                        <h5 class="card-title">{{$event->event_name}}</h5><span>
                        <p class="card-text">{{$event->event_description}}</p>
                        <div></div>
                        <span class="bg-dark p-2" style="color: white;">Event Address Details</span>
                        <p class="pt-3"><strong>Country : </strong><i>{{$event->event_country}}</i></p>
                        <p class="pt-3"><strong>Address : </strong><i>{{$event->event_address}}</i></p>
                        <p class="pt-3"><strong>Event Date : </strong><i>{{date('d-m-Y', strtotime($event->event_start_date))}} to {{date('d-m-Y', strtotime($event->event_end_date))}}</i></p>
                        <p class="card-text text-right"><small class="text-muted">{{ ($event->created_at->diffInMinutes(\Carbon\Carbon::now())) }} mins ago</small></p>
                       <h3 class="bg-dark" style="color: white">About Event </h3>
                        <p class="pt-3"><strong>No. Of Participants : </strong><i>{{$event->no_of_participent ? $event->no_of_participent : 'not known'}}</i></p>
                        <p class="pt-3"><strong>Min Age : </strong><i>{{$event->event_min_age}}</i></p>
                        <p class="pt-3"><strong>Max Age : </strong><i>{{$event->event_max_age}}</i></p>
                        <p class="pt-3"><strong>Event Url : </strong><i>{{$event->event_url ? $event->event_url:'not available'}}</i></p>
                        <p class="pt-3"><strong>Event Amount : </strong><i>{{$event->event_amount ? $event->event_amount:'not available'}}$</i></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(function () {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
$('#participateBtn').click(function (e) {
	   e.preventDefault();
        $.ajax({
  			data: $('#participate_form').serialize(),
          url: "{{ url('/add-event-participent') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {

              if(response.data.code == 200)
              {
 				$('#participateBtn').hide();
				$('#hiddenbtn').css("display", "block");
                 alert(response.data.message);
              }
              else if (response.data.code == 400)
              {
                alert(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
               alert(response.responseJSON.message);
          }
      });
    });
 });

</script>
@endsection
