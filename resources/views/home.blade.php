@extends('layouts.app')
@section('title', 'Create Event')
@section('styles')
    <style>

    </style>
@endsection
@section('content')
    <div class="container">

        @include('layouts.displayMessages')
        <div class="row">
            <div class="col-md-6 offset-3">
                <div class="card">
                    <div class="card-header">
                        <strong>All Events </strong>
                        <a href="{{route('event.create')}}" class="btn btn-primary" style="float: right">Add Event</a>
                    </div>
                    <div class="card-body">
                        @forelse($events as $event)
                            <div class="row pt-2">
                                <div class="col-md-3">
                                  <img src="{{env('IMAGE_PATH').'storage/app/public/'.$event->image}}" style="height: 150px; width: 100% ; margin-top: 10px; border-radius: 100%">
                                </div>
                                <div class="col-md-9">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$event->event_name}}</h5>
                                            <p class="card-text">{{Illuminate\Support\Str::limit($event->event_description,20)}}</p>
                                            <a href="{{route('event.detail',$event->id)}}" class="btn btn-primary">See Event Detail</a>
                                            <span class="" style="float: right; margin-top: 10px;margin-bottom: -10px"><strong>Created By: </strong> <i> {{(\App\Models\User::findOrFail($event->user_id))->name}}</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <h4 class="text-center">No Event to show</h4>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
