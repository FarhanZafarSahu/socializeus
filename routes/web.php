<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware(["verified"]);
Route::get('create-event',[App\Http\Controllers\EventController::class,'create'])->name('event.create');
Route::post('store-event',[App\Http\Controllers\EventController::class,'store'])->name('event.store');
Route::get('edit-event/{id}',[App\Http\Controllers\EventController::class,'edit'])->name('event.edit');
Route::post('update-event/{id}',[App\Http\Controllers\EventController::class,'update'])->name('event.update');

// comment
Route::post('add-comment',[App\Http\Controllers\CommentController::class,'store'])->name('comment.store');
Route::post('update-comment',[App\Http\Controllers\CommentController::class,'store'])->name('comment.update');
Route::get('delete-comment/{id}',[App\Http\Controllers\CommentController::class,'deleteComment'])->name('comment.delete');
Route::get('list-comment/{id}',[App\Http\Controllers\CommentController::class,'index'])->name('comment.list');
// event participent
Route::post('add-event-participent',[App\Http\Controllers\EventParticipentController::class,'store'])->name('event.participent.store');
Route::get('list-event-participent/{id}',[App\Http\Controllers\EventParticipentController::class,'index'])->name('event.participent.list');
Route::get('event-details/{id}',[App\Http\Controllers\EventController::class,'detail'])->name('event.detail');
